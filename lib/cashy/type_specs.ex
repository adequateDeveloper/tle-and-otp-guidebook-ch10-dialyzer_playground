defmodule Cashy.TypeSpecs do

  @spec add1(integer, integer) :: integer
  def add1(x, y) do
    x + y
  end

  @spec add2(integer | float, integer | float) :: integer | float
  def add2(x, y) do
    x + y
  end

  @spec add3(number, number) :: number
  def add3(x, y) do
    x + y
  end

  @spec foldl1([any], any, (any, any -> any)) :: any
  def foldl1(list, acc, function)
    when is_list(list) and is_function(function) do nil
      # the implementation is not important here
  end

  @spec foldl2([elem], acc, (elem, acc -> acc)) :: acc when elem: var, acc: var
  def foldl2(list, acc, function)
    when is_list(list) and is_function(function) do nil
    # the implementation is not important here
  end

  @spec map(f, list_1) :: list_2 when
    f: ((a) -> b),
    list_1: [a],
    list_2: [b],
    a: term,
    b: term
  def map(f, [h|t]), do: [f.(h)| map(f, t)]
  def map(f, []) when is_function(f, 1), do: []

end