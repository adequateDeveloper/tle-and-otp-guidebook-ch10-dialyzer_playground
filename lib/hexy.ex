defmodule Hexy do
  @type rgb() :: {0..255, 0..255, 0..255}                               # Type alias for an RGB color code
  @type hex() :: binary                                                 # Type alias for a Hex color code

  @spec rgb_to_hex1(rgb) :: hex                                   # Uses the custom type definitions in the specification
  def rgb_to_hex1({r, g, b}) do
    [r, g, b]
      |> Enum.map(fn x -> Integer.to_string(x, 16) |> String.rjust(2, ?0) end)
      |> Enum.join
  end

  @spec rgb_to_hex2(rgb) :: hex | {:error, :invalid}
  def rgb_to_hex2(rgb)                                                      # Bodiless function clause

  def rgb_to_hex2({r, g, b}) do
    [r, g, b]
      |> Enum.map(fn x -> Integer.to_string(x, 16) |> String.rjust(2, ?0) end)
      |> Enum.join
  end

  def rgb_to_hex2(_) do
    {:error, :invalid}
  end
end